#!/usr/bin/env python

# This is MorseCode 1.1
# Used for converting Morse To Text by @karthiksrivijay

FullText = raw_input("Please enter the text: ")
FullText = FullText.split("/")
Length = len(FullText)
Count = 0

while (Count == Length):
    if FullText[Count] == "._":
        FullText[Count] = "a"
    elif FullText[Count] == "_...":
        FullText[Count] = "b"
    elif FullText[Count] == "_._.":
        FullText[Count] = "c"
    elif FullText[Count] == "-..":
        FullText[Count] = "d"
    elif FullText[Count] == ".":
        FullText[Count] = "e"
    elif FullText[Count] == ".._.":
        FullText[Count] = "f"
    elif FullText[Count] == "__.":
        FullText[Count] = "g"
    elif FullText[Count] == "....":
        FullText[Count] = "h"
    elif FullText[Count] == "..":
        FullText[Count] = "i"
    elif FullText[Count] == ".___":
        FullText[Count] = "j"
    elif FullText[Count] == "_._":
        FullText[Count] = "k"
    elif FullText[Count] == "._..":
        FullText[Count] = "l"
    elif FullText[Count] == "__":
        FullText[Count] = "m"
    elif FullText[Count] == "_.":
        FullText[Count] = "n"
    elif FullText[Count] == "___":
        FullText[Count] = "o"
    elif FullText[Count] == ".__.":
        FullText[Count] = "p"
    elif FullText[Count] == "__._":
        FullText[Count] = "q"
    elif FullText[Count] == "._.":
        FullText[Count] = "r"
    elif FullText[Count] == "...":
        FullText[Count] = "s"
    elif FullText[Count] == "_":
        FullText[Count] = "t"
    elif FullText[Count] == ".._":
        FullText[Count] = "u"
    elif FullText[Count] == "..._":
        FullText[Count] = "v"
    elif FullText[Count] == ".__":
        FullText[Count] = "w"
    elif FullText[Count] == "_.._":
        FullText[Count] = "x"
    elif FullText[Count] == "_.__":
        FullText[Count] = "y"
    elif FullText[Count] == "__..":
        FullText[Count] = "z"
    elif FullText[Count] == "._._._":
        FullText[Count] = "."
    elif FullText[Count] == "__..__":
        FullText[Count] = ","
    elif FullText[Count] == "..__..":
        FullText[Count] = "?"
    elif FullText[Count] == "_.._.":
        FullText[Count] = "/"
    elif FullText[Count] == ".__._.":
        FullText[Count] = "@"
    elif FullText[Count] == ".____":
        FullText[Count] = "1"
    elif FullText[Count] == "..___":
        FullText[Count] = "2"
    elif FullText[Count] == "...__":
        FullText[Count] = "3"
    elif FullText[Count] == "...._":
        FullText[Count] = "4"
    elif FullText[Count] == ".....":
        FullText[Count] = "5"
    elif FullText[Count] == "_....":
        FullText[Count] = "6"
    elif FullText[Count] == "__...":
        FullText[Count] = "7"
    elif FullText[Count] == "___..":
        FullText[Count] = "8"
    elif FullText[Count] == "____.":
        FullText[Count] = "9"
    elif FullText[Count] == "_____":
        FullText[Count] = "0"
    else:
        FullText[Count] = "X"
    Count = Count + 1

Result = ''.join(FullText)
print Result
